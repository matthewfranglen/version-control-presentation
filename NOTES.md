Overview
--------

This will cover version control.

 * What is it?
 * What is it for?
 * What are the benefits?

### The Person Who Never Makes a Mistake Will Never Make Anything

Making mistakes is normal. You are paid to make mistakes because you are paid to make things.

### You can change your environment and habits to make making mistakes easier and safer.

Undo is better than Confirm.

### Version Control makes mistakes less costly

It is a big and complex system of undoing.

### GIT

We use git for version control.
Git is a decentralized version control system that is very fast.

Being decentralized is a core part of being fast.
Every copy of a repository is complete and can act as a server or a client.

### Your Machine

It's worth remembering that you will spend most of your time in your personal repository, on your machine.
So lets start there.

### Create a Repository

### Create a Commit

### Why Add Files?

Why add and then commit?
Commits are logical units of work.
You may do a lot of work and then pull it apart into multiple commits.
Commits can tell a story about how the code was developed.

### Create a Branch

### Change Branches

### Merge a Branch

### Delete a Branch

### Read the Status

### Visualize It

A large repository is a complicated place.
Whenever you feel that you want to know something about the repository you are working in, you should visualize it.
Command line output is no substitute.

Gitk
https://stackoverflow.com/questions/17582685/install-gitk-on-mac

(there are other things too if you don't like that)

### Git Hub

We use github to coordinate activity and act as an offsite backup.
Github lets you create repositories, clone them, fork them and create pull requests.

We do all of these things so I will cover each.

