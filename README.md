# Version Control

See it [here](https://matthewfranglen.gitlab.io/version-control-presentation/).

This was built using [reveal.js](https://github.com/hakimel/reveal.js)

## Cheat Sheet

GitHub provides a cheat sheet for common commands [here](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf).
It may be a good idea to print it for reference.

## Glossary

GitHub provides a glossary [here](https://help.github.com/articles/github-glossary/).
The git glossary can be viewed with `man gitglossary` or [here](https://mirrors.edge.kernel.org/pub/software/scm/git/docs/gitglossary.html).

## Requirements

### Tools

There are a few commands that are used during this presentation.
Please run these commands before starting:

```bash
brew update
brew install git
brew install hub
```

### Access

You need to be able to use github from the command line.

```
git clone git@github.com/git/git
```

(this is really just a test - you don't need this repository).

If you can't do this then check the following:

 * You have a github account at all. Go to https://github.com/ to sign up

 * You have ssh keys registered for your account.
   Go to https://github.com/settings/keys to check.
   You should see at least one entry in the _SSH Keys_ section.

 * You have them available on your machine.
   Look in the `.ssh` folder in your home directory.

 * You use them when talking to github.
   Read the `.ssh/config` file.
   If this is not present and you only have one set of keys then this is unlikely to be the problem.

 * You have loaded them.
   Try running `ssh-add ~/.ssh/<name-of-your-key-file>`.
   If this asks for a password that you can't remember then you may have to generate another set of keys.

#### New Github Keys

The first thing is to check you have a `.ssh` folder at all:

```
mkdir ~/.ssh
```

Then you need to generate the keys:

```
ssh-keygen -t rsa -b 4096 -f github
```

This will generate two files called `github` and `github.pub`.
You need to move them to your `.ssh` folder:

```
mv github github.pub ~/.ssh
```

Now you need to make sure you use these keys when talking to github.
Add the following to `~/.ssh/config` (make that file if it doesn't exist):

```
Host github.com
    User git
    IdentityFile ~/.ssh/github
```

Then you need to make sure that the permissions on the files and folders are correct:

```
chown -R $(id -u):$(id -g) ~/.ssh
chmod 700 ~/.ssh
chmod 600 ~/.ssh/*
```

The last thing is to tell github about this new key.
Open the `~/.ssh/github.pub` file and copy the content.
Then go to https://github.com/settings/keys and add the key to your list of keys.

After all this you should check if you can clone the repository:

```
git clone git@github.com/git/git
```

#### Seeking Help

If you have made it this far and it still doesn't work then you need to get help from others.
To get help ask in the #git or #shell channels.
Please run this command first and save the output:

```
ssh -vvvv github.com
```
